The easiest way to test IA addons in reallife:

<center>
  <br>
  <img src="web/deploy.png" style="border:1px solid #ddd;border-radius:5px;filter:brightness(0.98);"/><br>
</center>

Testcode however, is easily written using mixins. just create a file `test.gs` in your appscript project.

```javascript
function test(){

    ia
    .mixin('setupTestData', function(){
        ia
        .set('foo',      '123')
        .set('order.id', '23lkl234')
    })
    .mixin('testProcessForm',  function(){
        ia.processForm() // assumes function exist + sets ia.sent 
        if( !ia.sent ) throw 'not good'
    })

    // fire tests
    ia
    .setupTestData()
    .testProcessForm()

}
```

Now start the test by launching the test function:(){}

<center>
  <br>
  <img src="web/testing.png" style="border:1px solid #ddd;border-radius:5px;filter:brightness(0.98);"/><br>
</center>

> NOTE: make sure that `test.gs` is below `ai.gs` in your appscript project (example above assumes `ia` exist)

