### Q: is IA opensource?

> A: Yes (under CPAL license). However you need a <a href="mailto:leonvankammen@gmail.com" target="_blank">commercial license</a> if you want to remove the logo / branding.

### Q: how much time do I save writing addons in IA?

> A: A lot. Reduction in boilerplate code usually correlates to less maintenance and more stability.

### Q: is IA suitable for enterprise companies?

> A: It is **especially** suitable for enterprise companies: full control over sourcecode.

### Q: is IA for addons and non-addons?

> A: Yes. Simple appscripts can be written without using `.page(..)` dialogs/sidebars.

### Q: Why is IA not using jquery / react / vue / lit-html / XYZ?

> A: IA focuses on bridging appscript with addon-interfaces & webpages. The final html-stack is up to the developer.

### Q: Is IA a framework for appscript?

> A: Let's consider it a library, since it wraps verbose appscript-functions.
