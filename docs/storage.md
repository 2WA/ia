Easily store your data using `.store( )` in both appscript and html/js

```javascript
var ia = ia.ia({
  foo:"bar",                        // our data definition
  documents:[]                      // in appscript
})
.store( PropertiesService.getDocumentProperties() )
```

Now in both appscript and html/js you can store data using `save( )`:

```javascript
ia
.set('foo','Y')
.save()
```

> NOTE: If you want to store data into the current user, use this instead:

```javascript
.store( PropertiesService.getUserProperties(), "myapp" )
```
> NOTE: `myapp` is an identifier to prevent multiple IA apps from colliding data 

The following way of manipulating data is adviced using `.set( )`:

```javascript
ia
.on('set', console.log )
.set('foo.a.b', 'B'    )  // app.foo.a.b contains 'B'
.set('foo.a.x', 'L'    )  // app.foo.a.x contains 'L'
.save()
```

> NOTE: `.set( )` makes datamutations [eventable](#eventable) using `.on('set')`

