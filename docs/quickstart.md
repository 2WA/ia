The most enterprise-friendly way to get started:
<center>
  <br>
  <img src="web/scriptcopy.png" style="border:1px solid #ddd;border-radius:5px;filter:brightness(0.98);"/><br>
  <h2>click <a target="_blank" href="https://script.google.com/d/1Hh2T0PUawLVtXFISAjXpy6mrw5ZBWGX4EcWaxA7vosb2pDyFqcG3KlKn/edit">here</a> for the latest version</h2>
</center>

> Alternatively, IA can be used as a module (add `M1n510T6oUxqzAqipGDVBWzo9sTPQznDB` at Resources > Libraries).
