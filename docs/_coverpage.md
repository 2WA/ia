<style id="webmakerstyle">
#forest{
    position:absolute;
    width:100%;
    height:99vh;
    z-index:20;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background: linear-gradient(45deg,#ACF 0%,#FFF 25%, #799 48%,#09a 52%, #8CC 56%, #CCC 75%, #033 100%);
    background-size: 100% 100%;
    transform: scale(1.2) translate(-120px,-60px);
    -webkit-animation-name: forest-anim;
    -webkit-animation-duration: 8.500000s;
    -webkit-animation-iteration-count: infinite;
}
#dots{
    z-index:30;
    width:100%;
    height:99vh;
    opacity:0;
    position:absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background-image: radial-gradient(#0003 33%, #0000 33%);
    background-size: 4px 4px;
    -webkit-animation-name: dots-anim;
    -webkit-animation-duration: 3.00000s;
    -webkit-animation-iteration-count: infinite;
}

#logo {
  opacity:0.9;
  display: block;
  width: 25vh;
  height: 25vh;
  background: linear-gradient(120deg, #0AF, #0AF 0%, #FFF 55%, #CCC 100%);
  border-radius: 50%;
  border: 20px solid #EEE;
  -webkit-transform-style: preserve-3d;
  transform: translate(-50%, -50%) rotateX(-1.7rad) rotateY(0rad) rotateZ(1.9rad);
  transition: all 0.3s ease;
  filter: blur(5px);
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  -webkit-animation-name: logo-anim;
  -webkit-animation-duration: 8.500000s;
  -webkit-animation-iteration-count: infinite;
    
}

#bg {
  display: block;
  width: 300%;
  height: 300vh;
  position: absolute;
  -webkit-transform-style: preserve-3d;
  transform: translate(-50%, -50%) rotateX(-1.7rad) rotateY(0rad) rotateZ(1.9rad) translateZ(-430px) scale(3);
  background-size: 90px;
}

#iashape{
    clip-path: polygon(0% 20%, 31% 20%, 31% 79%, 64% 18%, 100% 80%, 31% 80%, 0% 80%);
    position:absolute;
    width:16vh;
    height:16vh;
    z-index:50;
    -webkit-transform-style: preserve-3d;
    transform: rotate(250deg) translate(11vh,-5vh);
    top:50%;
    left:50%;
    background-color:#F55;
    background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='28' height='49' viewBox='0 0 28 49'%3E%3Cg fill-rule='evenodd'%3E%3Cg id='hexagons' fill='%23FFFFFF' fill-opacity='0.1' fill-rule='nonzero'%3E%3Cpath d='M13.99 9.25l13 7.5v15l-13 7.5L1 31.75v-15l12.99-7.5zM3 17.9v12.7l10.99 6.34 11-6.35V17.9l-11-6.34L3 17.9zM0 15l12.98-7.5V0h-2v6.35L0 12.69v2.3zm0 18.5L12.98 41v8h-2v-6.85L0 35.81v-2.3zM15 0v7.5L27.99 15H28v-2.31h-.01L17 6.35V0h-2zm0 49v-8l12.99-7.5H28v2.31h-.01L17 42.15V49h-2z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E");
    filter:blur(0px);
    mix-blend-mode:difference;
    background-size:120px;
    background-position:0% 0%;
    -webkit-animation-name: iashape-anim;
    -webkit-animation-duration: 8.500000s;
    -webkit-animation-iteration-count: infinite;
}

.info:after {
  position: absolute;
  font-family: 'Rubik', sans-serif;
  width: 100%;
  height: 30vh;
  content:'';
  top: 80vh;
  text-align:center;
  left: 0;
  font-size:37px;
  font-weight:900;
  color:#FFF9;
  z-index:50;
  -webkit-text-stroke-width: 1px;
  -webkit-text-stroke-color: #0005;
  -webkit-animation-name: info-anim;
  -webkit-animation-duration: 10.00000s;
  -webkit-animation-iteration-count: infinite;
 }

#root {
  margin: 0 auto;
  width: 100%;
  height: 99vh;
  top: 0;
  left: 0;
  z-index:150;
  right: 0;
  bottom: 0;
  perspective: 420px;
  backface-visibility: hidden;
  transform-style: preserve-3d;
  position: absolute;
}

#cameraRot {
  position: absolute;
  transform: rotateX(1.570796rad) rotateY(0rad) rotateZ(0rad);
  -webkit-transform-style: preserve-3d;
  transform-style: preserve-3d;
  top: 50%;
  left: 50%;
  perspective-origin: 50% 50%;
  -webkit-animation-name: CameraRot-anim;
  -webkit-animation-duration: 8.500000s;
  -webkit-animation-iteration-count: infinite;
}

#cameraLoc {
  position: absolute;
  transform: translate3d(-0.139124px, -0.032957px, -2.806781px);
  -webkit-transform-style: preserve-3d;
  transform-style: preserve-3d;
  top: 50%;
  left: 50%;
  -webkit-animation-name: CameraLoc-anim;
  -webkit-animation-duration: 8.500000s;
  -webkit-animation-iteration-count: infinite;
}

/* Animation keyframes */
@-webkit-keyframes CameraRot-anim {
  0.00% {
    -webkit-transform: rotateX(1.570796rad) rotateY(0rad) rotateZ(0rad); }
  26.60% {
    -webkit-transform: rotateX(1.570796rad) rotateY(0rad) rotateZ(0rad); }
  29.56% {
    -webkit-transform: rotateX(1.865371rad) rotateY(-0.008153rad) rotateZ(0.451829rad); }
  59.11% {
    -webkit-transform: rotateX(1.865371rad) rotateY(-0.008153rad) rotateZ(0.451829rad); }
  62.07% {
    -webkit-transform: rotateX(1.838056rad) rotateY(-0.064227rad) rotateZ(-0.379085rad); }
  97.04% {
    -webkit-transform: rotateX(1.838056rad) rotateY(-0.064227rad) rotateZ(-0.379085rad); }
  99.00% {
    -webkit-transform: rotateX(1.570796rad) rotateY(0rad) rotateZ(0rad); } }

@-webkit-keyframes CameraLoc-anim {
  0.00% {
    -webkit-transform: translate3d(-5.564943px, 112.271233px, -1.318278px); }
  26.60% {
    -webkit-transform: translate3d(-5.078405px, 106.919317px, -1.318278px); }
  29.56% {
    -webkit-transform: translate3d(61.089706px, 118.384085px, -37.7549px); }
  59.11% {
    -webkit-transform: translate3d(64.981537px, 129.012575px, -38.539894px); }
  62.07% {
    -webkit-transform: translate3d(-47.458916px, 117.819862px, -38.4516px); }
  97.04% {
    -webkit-transform: translate3d(-55.226803px, 136.36692px, -40.426993px); }
  99.00% {
    -webkit-transform: translate3d(-5.564943px, 112.271233px, -1.318278px); } }

@-webkit-keyframes logo-anim {
  0.00% { 
      background: linear-gradient(120deg, #0AF, #0AF 0%, #FFF 55%, #CCC 100%);
      filter: blur(10px);
      width: 25vh;
      height: 25vh;
      border-radius:50%;
  }
  23.60% {
    border: 20px solid #EEE;
    filter: blur(0px);
    transform: translate(-50%, -50%) rotateX(-1.7rad) rotateY(0rad) rotateZ(1.9rad) scale(1); 
    border-radius:50%;
  }
  26.60% {
    border: 25px solid #aCF;
    filter: blur(10px);
    transform: translate(-50%, -50%) rotateX(4.3rad) rotateY(0rad) rotateZ(0rad) scale(0.5); 
    width: 25vh;
    height: 25vh;
    background: linear-gradient(120deg, #0AF, #0AF 0%, #FFF 55%, #CCC 100%);
  }
  29.56% {
    border: 20px solid #EEE;
    filter: blur(0px);
    transform: translate(-50%, -50%) rotateX(4.3rad) rotateY(0rad) scale(1); 
    background-image: url(web/before.png);
    background-color: #272822;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    border-radius:10px;
    width: 40vh;
    height: 33vh;
  }
  59.11% {
    border: 20px solid #EEE;
    background-image: url(web/before.png);
    transform: translate(-50%, -50%) rotateX(4.3rad) rotateY(0rad) scale(1); 
    background-image: url(web/before.png);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    width: 40vh;
    height: 33vh;
  }
  62.07% {
    transform: translate(-50%, -50%) rotateX(4.3rad) rotateY(0rad) scale(1); 
    border-radius:10px;
    filter: blur(0px); 
    background-image: url(web/after.png);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    width: 23vh;
    height: 33vh;
  }
  66% {
    border: 20px solid #EEE;
    filter: blur(0px); 
    background-image: url(web/after.png);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    transform: translate(-50%, -50%) rotateX(4.3rad) rotateY(0rad) scale(1); 
    border-radius:10px;
    width: 23vh;
    height: 33vh;
  }
  97.04% { 
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    width: 23vh;
    height: 33vh;
    border-radius:10px;
  }
  99.00% {
    background-image: url(web/after.png);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    filter: blur(0px); 
    width: 23vh;
    height: 33vh;
    transform: translate(-50%, -50%) rotateX(4.3rad) rotateY(0rad) rotateZ(0rad); 
  }
}

@-webkit-keyframes iashape-anim {
0.00% {
    opacity:1;
	background-position:0% 0%;
clip-path: polygon(0% 20%, 31% 20%, 31% 79%, 64% 18%, 100% 80%, 31% 80%, 0% 80%);
    
}
23.60% {
    opacity:1;
clip-path: polygon(0% 20%, 31% 20%, 31% 79%, 64% 18%, 100% 80%, 31% 80%, 0% 80%);
    
}
26.60% {
    opacity:0;
	clip-path: polygon(0 80%, 100% 19%, 0 19%, 100% 80%, 49% 31%, 100% 80%, 49% 79%);
}
29.56% {
clip-path: polygon(0% 20%, 31% 20%, 31% 79%, 64% 18%, 100% 80%, 31% 80%, 0% 80%);
}
59.11% {
	background-position:30% 30%;
clip-path: polygon(0% 20%, 31% 20%, 31% 79%, 64% 18%, 100% 80%, 31% 80%, 0% 80%);
    
}
62.07% {
		clip-path: polygon(0 80%, 100% 19%, 0 19%, 100% 80%, 49% 31%, 100% 80%, 49% 79%);
}
66%{
clip-path: polygon(0% 20%, 31% 20%, 31% 79%, 64% 18%, 100% 80%, 31% 80%, 0% 80%);
    
}
97.04% {
}
100.00% {
    opacity:0;
    background-position:0% 0%;
}
}

@-webkit-keyframes forest-anim {
  0.00% {
    transform: scale(1.6) translate(-80px,-60px);
      
  }
  23.60% {
    transform: scale(1.4) translate(-80px,-60px);
     }
  26.60% {
      transform: scale(1.4) translate(80px,60px);
    
  }
  29.56% {
      transform: scale(1.4) translate(80px,60px);
    
  }
  59.11% {
      transform: scale(1.3) translate(80px,60px);
  }
  62.07% {
    transform: scale(1.3) translate(-80px,60px);
  }
  66% {
    transform: scale(1.3) translate(-80px,60px);

  }
  97.04% {
    transform: scale(1.4) translate(-80px,60px);
      
  }
  100.00% {
    transform: scale(1.6) translate(-80px,-60px);
  }
}

@-webkit-keyframes info-anim {
  0% {
    opacity: 1;
    content:'isomorphic appscript (IA)'
  }
  49% {
    content:'isomorphic appscript (IA)'
    opacity: 0;
  }
  50%{
    opacity: 1;
    content:'less code more addons'
  }
  74%{
    opacity:0;
    content:'less code more addons'
  }
  75%{
      opacity:1;
      content:'write highlevel appscript'
  }
  100% {
      opacity:0;
      content:'write highlevel appscript'
  }
}
@-webkit-keyframes dots-anim {
    0%{
        opacity:0;
    }
    30.4%{
        opacity:0;
    }
    49.4%{
        opacity:1;
    }
    50%{
        opacity:1;
    }
    65%{
        opacity:0;
    }
    100%{
        opacity:0;
    }
}

@media only screen and (max-width: 600px) {
  #forest {
    width:160vh;
    height:110vh;
  }
  .info:after{
    font-size:26px;
  }
}

</style>
<!--
<div id="dots"></div>
<div id="forest"></div>
<div id="root">
	<div id="cameraRot">
		<div id="cameraLoc">
			<div id="bg"></div>
			<div id="stripes"></div>
			<div id="logo">
			    <div id="iashape"></div>
			</div>
		</div>
	</div>
</div>
<div class="info"></div>
-->

![color](#FFF0)
