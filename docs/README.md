<br><br><br>
IA is a little library which helps you write Google Appscript quicker.

Here's some chainable IA goodness:

```javascript
var app = ia.ia({
  foo:"bar",                        // setup persistent data
  documents:[]
})
.store( ... )                       // which (property)store?
.page("hello",   { ... })           // create sidebar/dialog from 'hello.html' template
.mixin('update', function(v){})     // expose isomorphic function
.on('something', function(v){})     // react to events
.update()                           // can be called from appscript or html
.emit('something', "i like turtles")

function onOpen(e){ app.showPage('hello') }
```

## Result

<center><img src="../web/after.png" style="border:1px solid #EEE;width:300px"/></center>

## isomorphic..iso-what?

IA lets you write code which is usable in both browser- and appscript-side.
This means hassle-free html interfaces for your appscripts.

