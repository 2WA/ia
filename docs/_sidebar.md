- Getting started

  - [Quick start](quickstart.md)
  - [Storing data](storage.md)
  - [Showing data](pages.md)
  - [Eventable data](eventable.md)
  - [Serving webpages](webpages.md)

- Reference

  - [All functions](reference.md)

<!---
[List of Plugins](plugins.md)
[Write a Plugin](write-a-plugin.md)
-->

- Guide

  - [FAQ](faq.md)
  - [Testing](testing.md)

- Contact
  - [Issues](https://gitlab.com/2WA/ia/-/issues)
  - [Support](mailto:incoming+2wa-ia-14143509-issue-@incoming.gitlab.com)
