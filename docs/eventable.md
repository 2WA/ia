React when stuff happens:

```javascript
ia
.on('foo',        console.log ) // prints 12

// somewhere else in the code
ia.emit('foo',12)
```

> NOTE: replace `console.log` with your own `function(v){ .. }`

The same works for mixins:


```javascript
ia
.mixin('hello', function(str){
  console.log('hello '+str)
})
.on('hello:before', console.log ) // prints 'world'
.on('hello',        console.log ) // prints 'world'

// somewhere else in the code
ia.hello('world')
```

Automatically save data when data changes:

```javascript
ia.on('set', ia.save  )

ia
.set('foo.a.b', 'X' )  // ia.save() called
.set('foo.a.b', 'Y' )  // ia.save() called
```

Write clean nested code:

```javascript
ia.set('foo.a.b', 'X'    )  // ia.foo.a.b now contains 'X'
ia.get('foo.a.b'         )  // returns 'X'
ia.get('foo.a.c', 'empty')  // returns 'empty' (because foo.a.c == undefined) 
```
