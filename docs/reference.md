## storage                          
|function|arguments|returns| info | 
|--------|---------|-------|------| 
| .get() | key, [fallback] | mixed | Get a nested variable: `.get('x.y.z', 3)` returns ia.x.y.z (if exists) otherwise 3|  
| .set() | key, [fallback] | mixed | Sets a nested variable: `.set('x.y.z', 3)` sets ia.x.y.z to 3|  
| .store() | [PropertyService](https://developers.google.com/apps-script/reference/properties/) |IA | specifies where to store data in |  
| .save() |  | IA | save data (`.data()`) to storage (`.store(..)`) | 
| .load() | | IA | load data into ia | 

## data manipulation                
|function|arguments|returns| info | 
|--------|---------|-------|------| 
| .pluck() | keys (array) | Object | Plucks only certain properties from ia-data|  
| .toArray() |  | array | converts ia object as array | 
| .clone() |  | IA | clones ia object | 
| .data() |  | Object | returns only data | 
| .template() | template "", vars {} | Function | evaluates es6 template literals. `var tpl = ia.template('${x}${y}',{x:1}); var str = tpl({y:1})` | 
| .schema() | key "",  jsonschema {} | IA | specifies schema for data-key (stored with `.save()`) for example `ia.schema('foo', {type:"string"})` | 
| .toSchema() | keys [] | IA | basically plucks a jsonschema from a set of data-properties. Handy for data-validation or form-generators. Example: `ia.toSchema['foo', 'bar'])` | 
| .xor() | newdata {} | IA | merges newdata into IA (like Object.assign()) but ignoring empty values in `newdata` (=XOR) | 

## helpers                          
|function|arguments|returns| info | 
|--------|---------|-------|------| 
| .log() | "" or {} | IA | isomorphic log which works both in html/js and appscript | 
| .each() | function(val, key) | IA | chainable way to map over objects and arrays. Basically `for(var i in x ) do(x[i],i); return ia` | 
| .then() | function(){} | IA | promise-like (but synchronous) function to make functioncalls chainable: `ia.then( functionA ).then( functionB )` | 
| .mixin() | functionname "",  function(){} | IA | register function which also emits events: `ia.mixin('loggy',console.log).on('loggy',functionB).loggy() | 
| .appscript() | fn, arg1, arg2, arg3 | promise | run appscript function in html/js: `ia.appscript('myFunction',1, 2).then(console.log).catch(console.log)` | 

## user interface                   
|function|arguments|returns| info | 
|--------|---------|-------|------| 
| .page() | htmlfile, config | IA | `.page('hello',{a:1})` registers a page (using hello.html) to show as dialog or sidemenu using `.showPage('hello')`|  
| .showPage() | htmlfile, ui | IA | `.showPage('hello', SpreadsheetApp.getUi())` registers a page (using hello.html) to show as dialog or sidemenu using `.showPage('hello')`|  
