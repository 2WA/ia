Webpages can be served `Express-style` using [Gexpress](https://github.com/coderofsalvation/Gexpress):

```javascript
ia
.mixin('showFoo', function(req, res, next){ 
    res.set('content-type','text/html')    
    res.send( '<h1>Hello world</h1>'  )
    res.end()
})

var server = new Gexpress.App()
server.get('/foo', ia.showFoo )

function doGet(e)         { return webapp.doGet(e)  }
function doPost(e)        { return webapp.doPost(e) }
```

> NOTE: To get the endpoints to work, You'll need to deploy the appscript as a webapp too. See [Gexpress](https://github.com/coderofsalvation/Gexpress) for more info & deployment instructions. 

### Displaying links

At what url will the endpoints above be served?

```javascript
ia
.set('rooturl', ScriptApp.getService().getUrl() )
.mixin('showFoo', function(req, res, next){ 
    var id   = req.query.id // contains 123
    var html = '<a href="${rooturl}?path=/foo&id=123">click here</a>'
    res.set('content-type','text/html')    
    res.send( ia.template(html,ia)() )
    res.end()
})
``` 

> TIP: use `.save( )` so IA can always access its root url in both appscript and html/js.
