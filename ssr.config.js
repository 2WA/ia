module.exports = {
  template: './index.html',
  maxAge: 60 * 60 * 1000,
  config: {
    //basePath: 'https://ia.isvery.ninja/',
    alias: {
      //'/de-de/changelog': '/changelog',
    },
    auto2top: true,
    coverpage: true,
    executeScript: true,
    loadSidebar: true,
    loadNavbar: true,
    mergeNavbar: true,
    maxLevel: 4,
    subMaxLevel: 2,
    name: 'docsify',
    search: {
      noData: {
        '/': 'No results!'
      },
      paths: 'auto',
      placeholder: {
        '/': 'Search'
      }
    }
  }
}
