/* CPAL LICENSE v1.0 
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at 2WA (leonvankammen@gmail.com).
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 have been added to cover use of software over a computer network and provide for limited attribution for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B.
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for the specific language governing rights and limitations under the License.
 * The Initial Developer of the Original Code is leonvankammen@gmail.com (2WA). All portions of the code written by 2WA are Copyright (c) 2019. All Rights Reserved.
 *
 * Please purchase a commercial license in case you want remove/replace branding code (=bagges, logos & weblinks to https://ia.isvery.ninja )
 * Contact leonvankammen@gmail.com to get a non-CPAL licensed version (and support sustainable opensource). 
 */
var iastr
function ia(o){
  
  var _ = {}
  
  _.unia = function(obj){
    for( var i in _ ) delete obj[i]
    delete obj.events
    delete obj.emit
    delete obj.on
    delete obj.cfg
    return obj
  }

  /* ## storage                          */
  /* |function|arguments|returns| info | */
  /* |--------|---------|-------|------| */
  
  /* | .get() | key, [fallback] | mixed | Get a nested variable: `.get('x.y.z', 3)` returns ia.x.y.z (if exists) otherwise 3|  */
  _.get = function get(obj,x,fallback) {
      obj = obj || this
      var o = String(x).split('.').reduce(function(acc, x) {
          if (acc == null || acc == undefined || acc == ''){
              return fallback;
          }
          return new Function("x","acc","return acc['" + x.split(".").join("']['") +"']" )(x, acc) || fallback
      }, obj)
      if( fallback && o == fallback ) _.set(obj,x,fallback)
      obj.emit('get', {key:x,value:o})
      return o
  }
  
  /* | .set() | key, [fallback] | mixed | Sets a nested variable: `.set('x.y.z', 3)` sets ia.x.y.z to 3|  */
  _.set = function set(obj, path, value) {
    var last
    var o = obj || this
    path = String(path)
    var vars = path.split(".")
    var lastVar = vars[vars.length - 1]
    vars.map(function(v) {
        if (lastVar == v) return
        o = (new Function("o","return o." + v)(o) || new Function("o","return o."+v+" = {}")(o))
        last = v
    })
    new Function("o","v","o." + lastVar + " = v")(o, value)
    obj.emit('set', {key:path,value:value})
    return ia( obj || {} )
  }
  
  /* | .store() | [PropertyService](https://developers.google.com/apps-script/reference/properties/) |IA | specifies where to store data in |  */
  _.store = function(ia,st,sk){
    if( sk ) ia.cfg.storekey = sk
    ia.cfg.store = st
    ia.load()
    return ia
  }

  /* | .save() |  | IA | save data (`.data()`) to storage (`.store(..)`) | */
  _.save= function(ia,k,v){
    if( ia.form ){                                               // 1. called by browser
      return ia.appscript('onUpdate', ia.pluck(ia.form).data() ) //    -> lets redirect to onUpdate (global func)
    }else{                                                       // 2. called in appscript
      if( k && v ) ia.set(k,v)
      ia.cfg.store.setProperty( ia.cfg.storekey, JSON.stringify(ia.data()) )
    }
    return ia
  }
  

  /* | .load() | | IA | load data into ia | */
  _.load= function(ia){
    ia.xor( JSON.parse( ia.cfg.store.getProperty( ia.cfg.storekey ) || '{}' ) )
    return ia
  }
  
  
  /*                                     */
  /* ## data manipulation                */
  /* |function|arguments|returns| info | */
  /* |--------|---------|-------|------| */
  
  
  /* | .pluck() | keys (array) | Object | Plucks only certain properties from ia-data|  */
  _.pluck = function pluck(obj,arr){
    var o = {}
    arr.map( function(l){ o[l] = obj.get(l) } )
    return ia(o)
  }
  
  /* | .toArray() |  | array | converts ia object as array | */
  _.toArray = function(o){
    if( o.push ) return o // already array
    var y = []
    for( var i in o ) y.push({key:i,value:o})
    return ia(y)
  }
  
  /* | .clone() |  | IA | clones ia object | */
  _.clone  = function(a){
    return ia( JSON.parse( JSON.stringify(a || this) ) )
  }
  
  /* | .data() |  | Object | returns only data | */
  _.data = function(o,d){
    if( !d ) return ia(o).clone().unia() // return clone without q functions
    else for( var i in d ) o[i] = d[i]
  }
  
  /* | .template() | template "", vars {} | Function | evaluates es6 template literals. `var tpl = ia.template('${x}${y}',{x:1}); var str = tpl({y:1})` | */
  _.template = function(ia,a,b){ // es6 style string evaluator
    var escape = function(b){ 
      var bb = JSON.parse( JSON.stringify(b) )
      for( var i in bb ) bb[ i.replace(/ /g,'_') ] = bb[i]
      return bb
    }
    var aa = a.replace(regex, function(x){
      return x.replace(/ /g,'_')   
    })
    var regex = /\${([^}]*)}/g    
    var data = function(b,c){
      var o = escape(b)
      if(c) for( var i in c ) o[i] = c[i]
      return o
    }
    return function(c){return String(aa).replace(regex,function(a,e){ return Function("x","try{ with(x)return "+e+" }catch(f){ return ''}").call(this,data(b,c))})}
  }
  
  /* | .schema() | key "",  jsonschema {} | IA | specifies schema for data-key (stored with `.save()`) for example `ia.schema('foo', {type:"string"})` | */
  _.schema = function(ia,k,schema){
    if( schema ) ia.cfg.schema[k] = schema
    return schema ? ia : (ia.cfg.schema[k] || {})
  }

  /* | .toSchema() | keys [] | IA | basically plucks a jsonschema from a set of data-properties. Handy for data-validation or form-generators. Example: `ia.toSchema['foo', 'bar'])` | */
  _.toSchema = function(ia,props){
    var schema = {type:"object",properties:{}}
    props.map( function(k){
      var v = ia.get(k)
      if( !v ) return
      var t = v.push ? "array" : typeof v
      var s = ia.schema(k)
      s.type  = s.type || t
      s.title = s.title || k 
      if( !s.format && s.type == "boolean" ) s.format = "checkbox"
      schema.properties[k.replace(/\./,'_')] = s
    })
    return schema
  }
  
  /* | .xor() | newdata {} | IA | merges newdata into IA (like Object.assign()) but ignoring empty values in `newdata` (=XOR) | */
  _.xor = function(o, sources) {
    var merge
    merge = function(target, sources,opts) {
      opts = opts || {}
      function isObject(item) {
        return (item && (typeof item === 'object' || typeof item == 'function') )
      }
      if (!sources.length) return target;
      const source = sources.shift();

      if (target && source ){
        for (var key in source) {
          var o = {}
          if (isObject(source[key])) {
            o[key] = {}
            if (!target[key]) target[key] = o[key]
            merge(target[key], [source[key]],opts);
          } else {
            target[key] = !opts.nonempty || source[key] ? source[key] : target[key]
          }
        }
      }
      return merge(target, sources,opts);
    }
    merge(o,sources,{nonempty:true})
    return o
  }

  /*                                     */
  /* ## helpers                          */
  /* |function|arguments|returns| info | */
  /* |--------|---------|-------|------| */
  
  /* | .log() | "" or {} | IA | isomorphic log which works both in html/js and appscript | */
  _.log = function(o,i){
    var str = typeof i == "string" ? i : JSON.stringify(i)
    if( typeof window == undefined ) Logger.log(str)
    console.log(str)
  }

  /* | .each() | function(val, key) | IA | chainable way to map over objects and arrays. Basically `for(var i in x ) do(x[i],i); return ia` | */
  _.each = function(obj,f){
    obj = obj || []
    for( var i in obj ){
      if( typeof obj[i] != 'function' ) f( obj[i], i )
    }
    return ia( obj || {} )
  }
  
  /* | .then() | function(){} | IA | promise-like (but synchronous) function to make functioncalls chainable: `ia.then( functionA ).then( functionB )` | */
  _.then = function(a,f){
    if( typeof f == "function" ){
    	if( f.then && !f.unia ) return f.then(a)
		f(a)
    }
    return a
  }
  
  
  _.eventemitter = function(o){
    o.events = {}
    o.emit = function(e,val){
      if( o.debug ) o.log("ia.emit('"+e+"')")
      var evs = o.events[e] || []
      evs.map( function(f){ if( f ) f(val) } )
      return o
    }
    o.on = function(e,f){
      var evs = o.events[e] || []
      evs.push(f)
      o.events[e] = evs
      return o
    }
  }
  
  /* | .mixin() | functionname "",  function(){} | IA | register function which also emits events: `ia.mixin('loggy',console.log).on('loggy',functionB).loggy() | */
  _.mixin = function(o,fn,f){
    o[fn] = function(){
      o.emit(fn+':before',arguments)
      f.apply(o,arguments)
      o.emit(fn,arguments)
      return o
    }
    return o
  }
  
  /* | .appscript() | fn, arg1, arg2, arg3 | promise | run appscript function in html/js: `ia.appscript('myFunction',1, 2).then(console.log).catch(console.log)` | */
  _.appscript = function appscript(fn,arg1,arg2,arg3){
    return new Promise( function(resolve,reject){
      console.log("appscript:"+fn+"("+arg1+","+JSON.stringify(arg2)+")" )
      google.script.run.withSuccessHandler( resolve )[fn](arg1,JSON.parse(JSON.stringify(arg2)),arg3)
    })
  }

    
  _.menu = function(ia,e){
    ia.emit('menu')
    return ia
  }

  /*                                     */
  /* ## user interface                   */
  /* |function|arguments|returns| info | */
  /* |--------|---------|-------|------| */

  /* | .page() | htmlfile, config | IA | `.page('hello',{a:1})` registers a page (using hello.html) to show as dialog or sidemenu using `.showPage('hello')`|  */
  _.page = function(ia,title,config){
    ia.cfg.pages = ia.cfg.pages || {}
    ia.cfg.pages[title] = config
    return ia
  }
  
  /* | .showPage() | htmlfile, ui | IA | `.showPage('hello', SpreadsheetApp.getUi())` registers a page (using hello.html) to show as dialog or sidemenu using `.showPage('hello')`|  */
  _.showPage = function(ia,title,ui){
    var cfg = ia.get('cfg.pages',{})[title]
    if( cfg ){
      var show = ia[ cfg.type ]
      if( !show ) throw 'no such function: ia.'+cfg.type+'(...)'   
      show(cfg.title || title, cfg.html || title, {ui:ui,cfg:cfg}) 
    }
    return ia
  }

  _.sidebar = function (ia, title,template,opts){
    opts = opts || {}
    var html = HtmlService.createTemplateFromFile(template)
    var data = ia.clone().data()
    data.cfg = ia.cfg
    for( var i in opts.cfg ) data[i] = opts.cfg[i]
    var tpldata  = {
      ia: iastr+'('+JSON.stringify(data)+')',
      css: HtmlService.createHtmlOutputFromFile(opts.cfg.css||"css").getContent(),
    }
    for( var i in opts ) tpldata[i] = typeof opts[i] == 'function' ? '' : opts[i]
    var html = ia.template( HtmlService.createTemplateFromFile(template).getRawContent(), ia.data() )(tpldata)
    var output = HtmlService.createTemplate(html)
    .evaluate()
    .setTitle( title )
    try{ opts.ui.showSidebar(output) }catch (e){ ia.log(e) } // ignore non-ui calls
    return ia
  }
  
  if( o && o.unia ) return o // already qified
  o.cfg    = o.cfg ? o.cfg : {storekey:"data", schema:{}}
  for( var i in _ ) o[i] = _[i].bind(o,o)
  o.eventemitter()
  return o
  
}
iastr = ia.toString()
